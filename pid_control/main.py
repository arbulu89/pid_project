"""
:author: Xabier Arbulu
:contact: arbulu89@gmail.com

:since: 2017-12-18
"""

import sys
import logging

import pid_control

def main(csv_file_path):
    """
    Main method
    """
    xPid = pid_control.PidControl(1.0, 0.1, 1.0)
    yPid = pid_control.PidControl(1.0, 0.1, 1.0)
    zPid = pid_control.PidControl(1.0, 0.1, 1.0)
    with open(csv_file_path) as data_file:
        header = data_file.readline()
        for line in data_file:
            t, x, y, z = [float(data) for data in line.split(",")]

            xPid.update(t, x, 0)
            yPid.update(t, y, 0)
            zPid.update(t, z, 0)

            logging.info("%f, %f, %f", xPid.output, yPid.output, zPid.output)
    return 0

if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO)
    main(sys.argv[1])
