"""
Unitary tests for pid_control.py.

:author: Xabier Arbulu
:contact: arbulu89@gmail.com

:since: 2017-12-18
"""

# pylint:disable=C0103,C0111,W0212,W0611

import logging
import unittest

import mock

from pid_control import pid_control

class TestPid(unittest.TestCase):
    """
    Unitary tests for PidControl.
    """

    @classmethod
    def setUpClass(cls):
        """
        Global setUp.
        """

        logging.basicConfig(level=logging.INFO)

    def setUp(self):
        """
        Test setUp.
        """
        self._pid = pid_control.PidControl(1, 2, 3)

    def tearDown(self):
        """
        Test tearDown.
        """

    @classmethod
    def tearDownClass(cls):
        """
        Global tearDown.
        """

    def test_output(self):
        self._pid._output = 5
        self.assertEqual(5, self._pid.output)

    def test_get_proportional(self):
        p = self._pid._get_proportional(10)
        self.assertEqual(10, p)

    def test_get_integral(self):
        self._pid._i_stored = 100
        d = self._pid._get_integral(10, 10, 9)
        self.assertEqual(220, d)

    def test_get_derivative(self):
        d = self._pid._get_derivative(10, 9, 20, 19)
        self.assertEqual(3, d)

    def test_get_derivative_error(self):
        d = self._pid._get_derivative(10, 9, 20, 20)
        self.assertEqual(0, d)

    def test_update(self):
        self._pid._last_time = 9
        self._pid._last_error = 20
        self._pid._get_proportional = mock.Mock(return_value=5)
        self._pid._get_integral = mock.Mock(return_value=6)
        self._pid._get_derivative = mock.Mock(return_value=7)
        output = self._pid.update(10, 19, 20)

        self.assertEqual(18, output)
        self.assertEqual(18, self._pid._output)

        self.assertEqual(10, self._pid._last_time)
        self.assertEqual(1, self._pid._last_error)

        self._pid._get_proportional.assert_called_once_with(
            1
        )
        self._pid._get_integral.assert_called_once_with(
            1, 10, 9
        )
        self._pid._get_derivative.assert_called_once_with(
            1, 20, 10, 9
        )
