"""
:author: Xabier Arbulu
:contact: arbulu89@gmail.com

:since: 2017-12-18
"""

import logging

class PidControl(object):
    """
    Class to control pid control algorithm

    Args:
        p (float): Proportional constant value
        i (float): Integral constant value
        d (float): Derivative constant value
    """

    def __init__(self, p, i, d, logger=None):
        self._logger = logger or logging.getLogger(__name__)
        self._p = p
        self._i = i
        self._i_stored = 0.0
        self._d = d
        self._last_time = 0.0
        self._last_error = 0.0
        self._output = 0.0

    @property
    def output(self):
        """
        Retrieve current output value (it makes this attribute public)

        Returns:
            output (float): Current output value
        """
        return self._output

    def _get_proportional(self, error):
        """
        Return new Proportional value

        Args:
            error (float): difference between desired and current values
        """
        return self._p * error

    def _get_integral(self, error, c_time, last_time):
        """
        Update and get integral value in time

        Formula:

        Int (e) ~ e (t_1) * (t_1 - t_0) + ... + e (t_1) * (t_n - t_n-1)

        Args:
            error (float): difference between desired and current values
            c_time (float): current time
            last_time (float): last time
        """
        self._i_stored += error * (c_time - last_time)
        return self._i * self._i_stored

    def _get_derivative(self, c_error, last_error, c_time, last_time):
        """
        Update and get derivative value in time

        Formula:

        {e (t_n) - e (t_n-1)} / (t_n - t_n-1)

        Args:
            c_error (float): current time error
            last_error (float): last time error
            c_time (float): current time
            last_time (float): last timte
        """
        try:
            d_value = (c_error - last_error) / (c_time - last_time)
            return self._d * d_value
        except ZeroDivisionError:
            return 0.0

    def update(self, c_time, current_value, desired_value):
        """
        Update Pid controller value with received data. Update value is stored
        in output variable.

        Used formula:

        (T) = p * e (t) + i * int (e) [0 - t] + d * e '(t)

        Args:
            c_time (float): Current time value
            current_value (float):  New input value
            desired_value (float): Desired value (threshold)
        """
        error = desired_value - current_value

        # Get new pid data
        p_value = self._get_proportional(error)
        i_value = self._get_integral(error, c_time, self._last_time)
        d_value = self._get_derivative(
            error, self._last_error, c_time, self._last_time)
        self._output = p_value + i_value + d_value

        self._last_time = c_time
        self._last_error = error

        # data returned just in case
        return self._output
