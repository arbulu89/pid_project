# PID control implementation
PID controller implementation.

# How to run

    cd alerion_test_python/pid_control
    C:\Python27\python main.py ./data/data.csv

# How to run UT

    C:\Python27\Scripts\pip.exe install pytest mock pytest_cov
    cd alerion_test_python
    C:\Python27\Scripts\py.test --cov=pid_control --cov-report html .
